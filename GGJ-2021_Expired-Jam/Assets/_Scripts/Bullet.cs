﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Bullet : MonoBehaviour
{
    public event EventHandler<Collision> OnHit;
    [SerializeField] private Rigidbody _rb = null;
    [SerializeField] private float _bulletSpeed = 5;
    [SerializeField] private int _damage = 20;
    [SerializeField] private float destroyAfterSecond = 10;
    [SerializeField] private float destroyDelay = 1;

    private bool _bulletHit = false;
    private bool _shotByPlayer = false;

    public UnityEvent OnBulletDestroy;

    public void Shoot(bool pShotByPlayer)
    {
        _rb.AddForce(transform.forward * _bulletSpeed, ForceMode.Force);
        _shotByPlayer = pShotByPlayer;
        StartCoroutine(destroyAfter());
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (_bulletHit) return;

        if(collision.gameObject.CompareTag("Enemy") && _shotByPlayer)
        {
            collision.gameObject.GetComponent<Enemy>().DealDamage(_damage);
            OnHit?.Invoke(this, collision);
            StartCoroutine(delayDestroy());
        }

        if(collision.gameObject.CompareTag("Player") && !_shotByPlayer)
        {
            PlayerManager shooter = collision.gameObject.GetComponent<PlayerManager>();
            // Adds negative bullets, aka substracts
            shooter.AddAmmo(PlayerManager.Bullets.red, -_damage);
            shooter.AddAmmo(PlayerManager.Bullets.green, -_damage);
            shooter.AddAmmo(PlayerManager.Bullets.blue, -_damage);
            OnHit?.Invoke(this, collision);
        }

        StartCoroutine(delayDestroy());
        _bulletHit = true;
    }

    private IEnumerator delayDestroy()
    {
        OnBulletDestroy?.Invoke();
        yield return new WaitForSeconds(destroyDelay);
        Destroy(gameObject);
    }

    private IEnumerator destroyAfter()
    {
        yield return new WaitForSeconds(destroyAfterSecond);
        StartCoroutine(delayDestroy());
    }
}
