﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Portal : MonoBehaviour
{
    [SerializeField] private string _levelName = "";
    private bool _loadScene = false;

    private void OnTriggerEnter(Collider other)
    {
        if (!_loadScene)
        {
            if (other.CompareTag("Player"))
            {
                SceneManager.LoadSceneAsync(_levelName);
                _loadScene = true;
            }
        }
    }
}
