﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ExplosionBullet : Bullet
{
    public UnityEvent OnExplode;
    [Header("Explosion Settings")]
    [SerializeField] private int _explodeAtChargesAmount = 10;
    [SerializeField] private float _explosionForce = 4;
    [SerializeField] private float _explosionRadius = 4;
    [SerializeField] private int _explosionDamage = 30;

    private bool _damageDealt = false;

    private void Start()
    {
        OnHit += OnBulletHit;
    }


    private void OnBulletHit(object pSender, Collision pCollision)
    {
        if (_damageDealt) return;
        EnemyPatrolChase enemy = pCollision.gameObject.GetComponent<EnemyPatrolChase>();
        if (enemy != null)
        {
            if(enemy.AddCharge(_explodeAtChargesAmount) >= _explodeAtChargesAmount)
            {
                enemy.ResetCharges();
                explode();             
            }

            _damageDealt = true;
        }
    }


    private void explode()
    {
        Rigidbody rb = GetComponent<Rigidbody>();
        Collider[] explosionHits = Physics.OverlapSphere(transform.position, _explosionRadius);

        foreach(Collider col in explosionHits)
        {
            Enemy enemyHit = col.gameObject.GetComponent<Enemy>();
            if(enemyHit != null)
            {
                enemyHit.DealDamage(_explosionDamage);
            }
        }

        rb.AddExplosionForce(_explosionForce, transform.position, _explosionRadius);
        OnExplode?.Invoke();
    }
}
