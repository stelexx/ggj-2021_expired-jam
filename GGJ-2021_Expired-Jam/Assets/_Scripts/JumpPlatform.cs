﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class JumpPlatform : MonoBehaviour
{
    [SerializeField] private Transform _direction = null;
    [SerializeField] private float _force = 10;

    public UnityEvent OnShoot;

    private void OnTriggerEnter(Collider other)
    {
        OnShoot?.Invoke();
        other.gameObject.GetComponent<PlayerMovement>()?.Launch(_direction.forward, _force);
    }
}
