﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public class EnemyPatrolChase : Enemy
{
    public UnityEvent OnSlow;

    protected enum EnemyState { patrolling, chasing }
    [SerializeField] protected EnemyState currentState = EnemyState.patrolling;

    [Header("Move Settings")]
    [SerializeField] private Transform _waypointHolder = null; // put transforms within this transform
    public float _detectRange = 10;
    protected Transform _player = null;

    private List<Transform> _waypoints;
    private NavMeshAgent _agent;

    [SerializeField] private int _currentWaypointIndex = 0;
    private bool _hasDestination = true;

    [Header("Speed Settings")]
    [SerializeField] private float _chaseSpeed = 3.5f;
    private float _originalChaseSpeed = 0;
    [SerializeField] private float _patrollingSpeed = 2f;
    private float _originalPatrolllingSpeed = 0;

    [SerializeField] private float _slowTimer = 0;
    [SerializeField] private float _slowDuration = 0;
    [SerializeField] private bool _isSlowed = false;


    void Awake()
    {
        _agent = GetComponent<NavMeshAgent>();
        _player = GameObject.FindGameObjectWithTag("Player").transform;
        getWaypoints();
        _originalChaseSpeed = _chaseSpeed;
        _originalPatrolllingSpeed = _patrollingSpeed;
    }


    new protected void Update()
    {
        base.Update();
        switch (currentState)
        {
            case EnemyState.chasing:
                chasing();
                break;

            case EnemyState.patrolling:
                patrolling();
                break;
        }

        if (dead) _agent.isStopped = true;
        updateSlow();
    }


    public void Slow(float pDuration, float pPercentage)
    {
        Debug.Log("Slow: " + _isSlowed);

        if (_isSlowed)
        {
            _chaseSpeed = 0;
            _patrollingSpeed = 0;
            Debug.Log("Enemy freeze!");
        }
        else
        {
            _chaseSpeed *= 1 - pPercentage;
            _patrollingSpeed *= 1 - pPercentage;
            Debug.Log("Enemy slowed!");
        }

        _slowTimer = 0;
        _slowDuration = pDuration;
        _isSlowed = true;
        updateAgentSpeed();
        OnSlow?.Invoke();
    }


    protected float getDistanceToPlayer()
    {
        return (transform.position - _player.position).magnitude;
    }


    private void chasing()
    {
        _agent.destination = _player.position;
        if (!_hasDestination) // set destination
        {
            _hasDestination = true;
            updateAgentSpeed();
        }

        if (getDistanceToPlayer() > _detectRange) // player out of range -> patrolling
        {
            _hasDestination = false;
            currentState = EnemyState.patrolling;
        }
    }


    private void patrolling()
    {
        if (!_hasDestination || _agent.remainingDistance < _agent.stoppingDistance) // set destination
        {
            nextWaypoint();
        }

        _agent.destination = _waypoints[_currentWaypointIndex].position;

        if (getDistanceToPlayer() < _detectRange) // player within range -> chasing
        {
            _hasDestination = false;
            currentState = EnemyState.chasing;
        }
    }


    private void nextWaypoint()
    {
        _currentWaypointIndex++;
        if (_currentWaypointIndex > _waypoints.Count - 1) _currentWaypointIndex = 0;
        _agent.destination = _waypoints[_currentWaypointIndex].position;
        _hasDestination = true;
        updateAgentSpeed();
    }


    private void getWaypoints()
    {
        _waypoints = new List<Transform>();
        foreach (Transform trans in _waypointHolder)
        {
            _waypoints.Add(trans);
        }
    }


    private void updateSlow()
    {
        if (_isSlowed)
        {
            _slowTimer += Time.deltaTime;

            if (_slowTimer > _slowDuration)
            {
                _chaseSpeed = _originalChaseSpeed;
                _patrollingSpeed = _originalPatrolllingSpeed;
                _isSlowed = false;
            }
        }
    }

    private void updateAgentSpeed()
    {
        switch (currentState)
        {
            case EnemyState.chasing:
                _agent.speed = _chaseSpeed;
                break;
            case EnemyState.patrolling:
                _agent.speed = _patrollingSpeed;
                break;
        }
    }
}
