﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MeleeAI : EnemyPatrolChase
{
    [Header("Melee Settings")]
    [SerializeField] private float _meleeRange = 3;
    [SerializeField] private float _timeBetweenAttack = 1;

    [SerializeField] private int _damagePerAttack = 30;
    public UnityEvent OnMeleeAttack;

    private PlayerManager _playerShooter = null;

    private float attackTimer = 0;

    private void Start()
    {
        _playerShooter = FindObjectOfType<PlayerManager>();
    }


    new void Update()
    {
        base.Update();

        if (getDistanceToPlayer() < _meleeRange)
        {
            attack();
        }

        attackTimer += Time.deltaTime;
    }


    private void attack()
    {
        if (attackTimer > _timeBetweenAttack)
        {
            attackTimer = 0;
            damagePlayer();
            OnMeleeAttack?.Invoke();
        }
    }


    private void damagePlayer()
    {
        // Adds negative bullets, aka substracts
        _playerShooter.AddAmmo(PlayerManager.Bullets.red, -_damagePerAttack);
        _playerShooter.AddAmmo(PlayerManager.Bullets.green, -_damagePerAttack);
        _playerShooter.AddAmmo(PlayerManager.Bullets.blue, -_damagePerAttack);
    }
}
