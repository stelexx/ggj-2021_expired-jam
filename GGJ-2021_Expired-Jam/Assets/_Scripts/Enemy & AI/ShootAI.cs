﻿using UnityEngine;
using UnityEngine.Events;

public class ShootAI : EnemyPatrolChase
{
    [Header("Shoot Settings")]
    [SerializeField] private float _minimumShootDistance = 10;
    [SerializeField] private float _timeBetweenAttacks = 2;

    [SerializeField] private GameObject _bulletPrefab = null;
    [SerializeField] private Transform _shootPosition = null;
    [SerializeField] private float _sphereCastRadius = 1;
    [SerializeField] private float _shootSpeed = 5;
    public UnityEvent OnShoot;

    private float attackTimer = 0;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    new void Update()
    {
        base.Update();
        if (currentState == EnemyState.chasing)
        {
            shoot();
        }

        attackTimer += Time.deltaTime;
    }

    private void shoot()
    {
        if (attackTimer > _timeBetweenAttacks)
        {
            RaycastHit hit;
            _shootPosition.LookAt(_player, Vector3.up);
            _shootPosition.localEulerAngles = new Vector3(_shootPosition.localEulerAngles.x, 0, 0);
            if (Physics.SphereCast(_shootPosition.position, _sphereCastRadius, _shootPosition.forward, out hit, _minimumShootDistance))
            {
                if (hit.collider.CompareTag("Player"))
                {
                    Debug.Log("Sphere cast hit player");
                    attackTimer = 0;
                    GameObject bullet = Instantiate(_bulletPrefab, _shootPosition.position, _shootPosition.rotation);
                    bullet.GetComponent<Bullet>().Shoot(false);
                    OnShoot?.Invoke();
                }
            }
        }
    }
}
