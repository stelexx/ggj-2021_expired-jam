﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Enemy : MonoBehaviour
{
    [Header("Health")]
    public UnityEvent OnDie;
    public UnityEvent OnDamage;
    [SerializeField] private int health = 100;
    [SerializeField] private float _destroyDelay = 1;
    [SerializeField] private float _removeChargeEverySeconds = 5;
    protected bool dead = false;

    [SerializeField] private int explosionChargeCount = 0;
    private float _chargedTimer = 0;

    [SerializeField] private MeshRenderer _eyes;
    private Material _shader = null;
    [SerializeField] private Vector2 _minMaxeyeColorChange = new Vector2();
    private int _maxCharges = 999;

    public void DealDamage(int pAmount)
    {
        health -= pAmount;
        OnDamage?.Invoke();
        if (health <= 0 && !dead)
        {
            OnDie?.Invoke();
            StartCoroutine(delayDestroy());
            dead = true;
        }
    }


    public int AddCharge(int pMaxCharges)
    {
        _maxCharges = pMaxCharges;
        explosionChargeCount++;
        _chargedTimer = 0;
        updateEyeColor();
        return explosionChargeCount;
    }


    public void ResetCharges()
    {
        explosionChargeCount = 0;
        updateEyeColor();
    }


    protected void Update()
    {
        _chargedTimer += Time.deltaTime;
        if (_chargedTimer > _removeChargeEverySeconds)
        {
            explosionChargeCount--;
            if (explosionChargeCount < 0) explosionChargeCount = 0;
            _chargedTimer = 0;
            updateEyeColor();
        }
    }


    private IEnumerator delayDestroy()
    {
        yield return new WaitForSeconds(_destroyDelay);
        Destroy(gameObject);
    }

    private void updateEyeColor()
    {
        float e = explosionChargeCount;
        float m = _maxCharges;
        float scale = e / m;
        if (explosionChargeCount == _maxCharges) scale = 1;

        float eyesColorStrength = Mathf.Lerp(_minMaxeyeColorChange.x, _minMaxeyeColorChange.y, scale);
        Debug.Log("eyes color: " + eyesColorStrength);

        _eyes.material.SetFloat("Eyes", eyesColorStrength);
    }
}
