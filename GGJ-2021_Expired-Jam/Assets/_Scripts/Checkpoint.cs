﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    private CheckpointManager _checkpointManager = null;

    private void Start()
    {
        _checkpointManager = FindObjectOfType<CheckpointManager>();
    }


    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            _checkpointManager.SaveCheckpoint(this);
        }
    }
}
