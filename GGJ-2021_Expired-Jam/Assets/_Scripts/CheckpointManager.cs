﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CheckpointManager : MonoBehaviour
{
    public UnityEvent OnPortalEnter;
    [SerializeField] private Transform _lastCheckpoint = null;
    [SerializeField] private Transform _blackRoom = null;
    [SerializeField] private CharacterController _player = null;
    [SerializeField] private GameObject _ui;

    [Header("Ammo Reset settings")]
    [SerializeField] private int _blueAmmo = 10;
    [SerializeField] private int _greenAmmo = 10;
    [SerializeField] private int _redAmmo = 10;

    public void PlayerToRoom()
    {
        _ui.SetActive(false);
        _player.enabled = false;
        _player.transform.position = _blackRoom.position;
        _player.transform.rotation = _blackRoom.rotation;
        _player.enabled = true;
    }


    public void PlayerToLastCheckpoint()
    {
        _player.GetComponent<PlayerManager>().SetAmmo(_redAmmo, _greenAmmo, _blueAmmo);
        OnPortalEnter?.Invoke();
        _ui.SetActive(true);
        _player.enabled = false;
        Debug.Log("Transport player to last checkpoint: " + _lastCheckpoint.position);
        _player.transform.position = _lastCheckpoint.position;
        _player.transform.rotation = _lastCheckpoint.rotation;
        _player.enabled = true;
    }


    public void SaveCheckpoint(Checkpoint pCheckpoint)
    {
        Debug.Log("Save checkpoint: " + pCheckpoint.gameObject.name);
        _lastCheckpoint = pCheckpoint.transform;
    }
}
