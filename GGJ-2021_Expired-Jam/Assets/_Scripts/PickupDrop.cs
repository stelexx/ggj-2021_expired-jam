﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupDrop : MonoBehaviour
{
    [Header("Pickup Prefabs")]
    [SerializeField] private GameObject _redPickup = null;
    [SerializeField] private GameObject _greenPickup = null;
    [SerializeField] private GameObject _bluePickup = null;

    [Header("Drop percentage")]
    [SerializeField] private int _redPercentage = 33;
    [SerializeField] private int _greenPercentage = 33;
    [SerializeField] private int _bluePercentage = 33;


    public void DropPickup()
    {
        int randomNumber = Random.Range(1, 101);
        if (randomNumber < _redPercentage) drop(_redPickup);
        else if (randomNumber < _greenPercentage + _redPercentage) drop(_greenPickup);
        else if (randomNumber < _greenPercentage + _redPercentage + _bluePercentage) drop(_bluePickup);
    }


    private void drop(GameObject pPrefab)
    {
        Instantiate(pPrefab, transform.position, Quaternion.identity);
    }
}
