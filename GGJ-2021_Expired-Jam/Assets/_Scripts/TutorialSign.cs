﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialSign : MonoBehaviour
{
    [SerializeField] private GameObject _signObject = null;

    private void OnTriggerExit(Collider other)
    {
        Debug.Log("On trigger exit");
        _signObject.SetActive(false);
    }
}
