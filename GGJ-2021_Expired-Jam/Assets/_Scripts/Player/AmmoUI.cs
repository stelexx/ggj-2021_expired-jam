﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AmmoUI : MonoBehaviour
{
    private PlayerManager _shooter = null;

    [Header("UI Elements")]
    [SerializeField] private TextMeshProUGUI _selectedAmmo = null;
    [SerializeField] private TextMeshProUGUI _firstAmmo = null;
    [SerializeField] private TextMeshProUGUI _secondAmmo = null;


    [Header("Color Circles")]
    [SerializeField] private Image _selectedCircle = null;
    [SerializeField] private Image _firstCircle = null;
    [SerializeField] private Image _secondCircle = null;
    [SerializeField] private Image _bar = null;


    void Start()
    {
        _shooter = FindObjectOfType<PlayerManager>();
        _shooter.OnWeaponChange.AddListener(onWeaponChange);
        onWeaponChange();
    }


    void Update()
    {
        updateUI();
    }


    private void updateUI()
    {
        _selectedAmmo.text = getAmmoAmount(_shooter.bulletSelected).ToString();
        _firstAmmo.text = getAmmoAmount(getFirstColor()).ToString();
        _secondAmmo.text = getAmmoAmount(getSecondColor()).ToString();
        updateBar(getAmmoAmount(_shooter.bulletSelected));
    }

    private void onWeaponChange()
    {
        updateColors();
        updateUI();
    }


    private void updateColors()
    {
        _selectedCircle.color = getColor(_shooter.bulletSelected);
        _firstCircle.color = getColor(getFirstColor());
        _secondCircle.color = getColor(getSecondColor());
        _bar.color = getColor(_shooter.bulletSelected);
    }


    private PlayerManager.Bullets getFirstColor()
    {
        int color = (int)_shooter.bulletSelected + 1;
        if (color > 2) color = 0;
        return (PlayerManager.Bullets)color;
    }

    private PlayerManager.Bullets getSecondColor()
    {
        int color = (int)_shooter.bulletSelected + 1;
        if (color > 2) color = 0;
        color += 1;
        if (color > 2) color = 0;
        return (PlayerManager.Bullets)color;
    }


    private int getAmmoAmount(PlayerManager.Bullets pBullet)
    {
        switch(pBullet)
        {
            case PlayerManager.Bullets.red:
                return _shooter.redAmmo;
            case PlayerManager.Bullets.green:
                return _shooter.greenAmmo;
            case PlayerManager.Bullets.blue:
                return _shooter.blueAmmo;
        }

        return 0;
    }


    private Color getColor(PlayerManager.Bullets pColor)
    {
        switch (pColor)
        {
            case PlayerManager.Bullets.red:
                return Color.red;
            case PlayerManager.Bullets.blue:
                return Color.blue;
            case PlayerManager.Bullets.green:
                return Color.green;
        }

        return Color.white;
    }


    private void updateBar(int pAmmo)
    {
        _bar.fillAmount = ((float)pAmmo / 255);
    }
}
