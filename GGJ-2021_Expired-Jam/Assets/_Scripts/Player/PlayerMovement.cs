﻿using UnityEngine;
using UnityEngine.Events;

public class PlayerMovement : MonoBehaviour
{
    public UnityEvent OnJump;

    [Header("Movement Settings")]
    [SerializeField] private float _moveSpeed = 5;
    [SerializeField] private float _runSpeed = 8;
    [SerializeField] private float _jumpForce = 10;
    [SerializeField] private float gravity = 20.0f;

    [Header("Aim Settings")]
    [SerializeField] private Transform _cameraObject = null;
    public float aimSensitivity = 1;
    [SerializeField] private float lookXLimit = 45.0f;

    private CharacterController _characterController;
    private Vector3 moveDirection = Vector3.zero;
    private float rotationX = 0;


    private void Start()
    {
        _characterController = GetComponent<CharacterController>();

        // Lock cursor
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }


    void Update()
    {
        move();
        aim();
    }


    public void Launch(Vector3 pDirection, float pSpeed)
    {
        moveDirection = pDirection * pSpeed;
    }


    private void move()
    {
        Vector3 forward = transform.TransformDirection(Vector3.forward);
        Vector3 right = transform.TransformDirection(Vector3.right);

        float moveSpeed = Input.GetKey(KeyCode.LeftShift) ? _runSpeed : _moveSpeed;
        float xSpeed = moveSpeed * Input.GetAxis("Vertical");
        float ySpeed = moveSpeed * Input.GetAxis("Horizontal");
        float movementDirectionY = moveDirection.y;
        moveDirection = (forward * xSpeed) + (right * ySpeed);

        if (Input.GetButton("Jump") && _characterController.isGrounded)
        {
            moveDirection.y = _jumpForce;
            OnJump?.Invoke();
        }
        else moveDirection.y = movementDirectionY;

        if(!_characterController.isGrounded) moveDirection.y -= gravity * Time.deltaTime;

        _characterController.Move(moveDirection * Time.deltaTime);
    }


    private void aim()
    {
        rotationX += -Input.GetAxis("Mouse Y") * aimSensitivity;
        rotationX = Mathf.Clamp(rotationX, -lookXLimit, lookXLimit);
        _cameraObject.localRotation = Quaternion.Euler(rotationX, 0, 0);
        transform.rotation *= Quaternion.Euler(0, Input.GetAxis("Mouse X") * aimSensitivity, 0);
    }
}
