﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using EZCameraShake;

public class PlayerManager : MonoBehaviour
{
    public UnityEvent OnShoot;
    public UnityEvent OnWeaponChange;
    public UnityEvent OnAmmoChange;
    public UnityEvent OnDie;

    public enum Bullets { red, green, blue };
    public Bullets bulletSelected = Bullets.red;

    [SerializeField] private Transform _shootPosition;


    [Header("Green Settings")]
    [SerializeField] private GameObject _greenBullet = null;
    public int greenAmmo = 255;
    [SerializeField] private int _greenAmmoCost = 10;
    [SerializeField] private float _greenTimeBetweenShots = 0.6f;

    [Header("Red Settings")]
    [SerializeField] private GameObject _redBullet = null;
    public int redAmmo = 255;
    [SerializeField] private int _redAmmoCost = 1;
    [SerializeField] private float _redTimeBetweenShots = 0.6f;

    [Header("Blue Settings")]
    [SerializeField] private GameObject _blueBullet = null;
    public int blueAmmo = 255;
    [SerializeField] private int _blueAmmoCost = 5;
    [SerializeField] private float _blueTimeBetweenShots = 0.6f;


    private float _shotTimer = 0;
    private CheckpointManager _checkpointManager = null;
    private PostProcessingChagner _postProcessingChanger = null;

    private void Start()
    {
        _checkpointManager = FindObjectOfType<CheckpointManager>();
        _postProcessingChanger = FindObjectOfType<PostProcessingChagner>();
    }


    // Update is called once per frame
    void Update()
    {
        updateShooting();
        updateWeaponSelection();
    }


    public void AddAmmo(Bullets pType, int pAmmount)
    {
        switch (pType)
        {
            case Bullets.red:
                redAmmo += pAmmount;
                redAmmo = Mathf.Clamp(redAmmo, 0, 255);
                break;
            case Bullets.green:
                greenAmmo += pAmmount;
                greenAmmo = Mathf.Clamp(greenAmmo, 0, 255);
                break;
            case Bullets.blue:
                blueAmmo += pAmmount;
                blueAmmo = Mathf.Clamp(blueAmmo, 0, 255);
                break;
        }

        if (pAmmount < 0) _postProcessingChanger.FadeVignetteInAndOut();

        if (redAmmo <= 0 && greenAmmo <= 0 & blueAmmo <= 0) playerDead();
        OnAmmoChange?.Invoke();
    }


    public void playerDead()
    {
        OnDie?.Invoke();
        Debug.Log("Player Dead");
        _checkpointManager.PlayerToRoom();
    }


    public void SetAmmo(int pRed, int pGreen, int pBlue)
    {
        redAmmo = pRed;
        greenAmmo = pGreen;
        blueAmmo = pBlue;
    }


    private void updateWeaponSelection()
    {
        if (Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            bulletSelected -= (int)(Input.GetAxis("Mouse ScrollWheel") * 10);

            if ((int)bulletSelected < 0) bulletSelected = (Bullets)2;
            if ((int)bulletSelected > 2) bulletSelected = (Bullets)0;

            OnWeaponChange?.Invoke();
        }

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            bulletSelected = Bullets.red;
            OnWeaponChange?.Invoke();
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            bulletSelected = Bullets.green;
            OnWeaponChange?.Invoke();
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            bulletSelected = Bullets.blue;
            OnWeaponChange?.Invoke();
        }
    }


    private void updateShooting()
    {
        if (Input.GetMouseButtonDown(0))
        {
            switch (bulletSelected)
            {
                case Bullets.red:
                    shootRed();
                    break;
                case Bullets.blue:
                    shootBlue();
                    break;
            }
        }

        if (Input.GetMouseButton(0))
        {
            if (bulletSelected == Bullets.green)
            {
                shootGreen();
            }
        }

        _shotTimer += Time.deltaTime;
    }


    private void shootBlue()
    {
        if (blueAmmo < _blueAmmoCost || _shotTimer <= _blueTimeBetweenShots) return;
        shoot(_blueBullet);
        blueAmmo -= _blueAmmoCost;
    }


    private void shootRed()
    {
        if (redAmmo < _redAmmoCost || _shotTimer <= _redTimeBetweenShots) return;
        shoot(_redBullet);
        redAmmo -= _redAmmoCost;
    }


    private void shootGreen()
    {
        if (greenAmmo < _greenAmmoCost || _shotTimer <= _greenTimeBetweenShots) return;
        shoot(_greenBullet);
        greenAmmo -= _greenAmmoCost;

    }


    private void shoot(GameObject pBullet)
    {
        GameObject bullet = Instantiate(pBullet, _shootPosition.position, _shootPosition.rotation);
        bullet.GetComponent<Bullet>().Shoot(true);
        OnShoot?.Invoke();
        OnAmmoChange?.Invoke();
        _shotTimer = 0;
    }
}
