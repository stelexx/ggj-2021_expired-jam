﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class PostProcessingChagner : MonoBehaviour
{
    private Volume _ppVolume = null;
    private ChannelMixer _channelMixer = null;
    private ColorAdjustments _colorAdjustments = null;
    private PlayerManager _shooter = null;
    private Vignette _vignette = null;

    [SerializeField] private int _red;
    [SerializeField] private int _blue;
    [SerializeField] private int _green;

    [SerializeField] private Vector2 minMaxSaturation = new Vector2(-100, 100);

    [Header("vignette animation")]
    [SerializeField] private float _vignetteFadeStrength = 2;
    [SerializeField] private float _vignetteFadeInDuration = 1;
    [SerializeField] private float _vignetteFadeOutDuration = 1;
    float startIntensity = 0;

    // Start is called before the first frame update
    void Start()
    {
        _ppVolume = FindObjectOfType<Volume>();
        _ppVolume.profile.TryGet(out _channelMixer);
        _ppVolume.profile.TryGet(out _colorAdjustments);
        _ppVolume.profile.TryGet(out _vignette);
        startIntensity = _vignette.intensity.value;
        _shooter = FindObjectOfType<PlayerManager>();
        _shooter.OnAmmoChange.AddListener(updateColors);

        updateColors();
    }


    private void updateColors()
    {
        int red = _shooter.redAmmo;
        int green = _shooter.greenAmmo;
        int blue = _shooter.blueAmmo;
        _channelMixer.redOutRedIn.value = 100 + (((float)red / 255) * 100);
        _channelMixer.blueOutBlueIn.value = 100 + (((float)blue / 255) * 100);
        _channelMixer.greenOutGreenIn.value = 100 + (((float)green / 255) * 100);

        float saturationScale = (float)(red + green + blue) / (3 * 255);
        _colorAdjustments.saturation.value = Mathf.Lerp(minMaxSaturation.x, minMaxSaturation.y, saturationScale);
        _colorAdjustments.colorFilter.value = new Color(red, green, blue);
    }


    public void FadeVignetteInAndOut()
    {
        StartCoroutine(vignetteFade());
    }


    private IEnumerator vignetteFade()
    {
        float timer = 0;

        while(timer < _vignetteFadeInDuration)
        {
            timer += Time.deltaTime;
            _vignette.intensity.value = Mathf.Lerp(startIntensity, _vignetteFadeStrength, timer / _vignetteFadeInDuration);
            yield return null;
        }

        timer = 0;

        while (timer < _vignetteFadeOutDuration)
        {
            timer += Time.deltaTime;
            _vignette.intensity.value = Mathf.Lerp(_vignetteFadeStrength, startIntensity, timer / _vignetteFadeOutDuration);
            yield return null;
        }
    }
}
