﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class AmmoPickup : MonoBehaviour
{
    public UnityEvent OnPickup;

    [SerializeField] private PlayerManager.Bullets _ammoType = PlayerManager.Bullets.red;
    [SerializeField] private int _ammoAmount;
    [SerializeField] private float _destroyDelay = 1;
    private PlayerManager _shooter = null;

    private bool _pickedUp = false;

    private void Start()
    {
        _shooter = FindObjectOfType<PlayerManager>();
    }


    private void OnTriggerEnter(Collider other)
    {
        if (!_pickedUp && other.CompareTag("Player"))
        {
            _shooter.AddAmmo(_ammoType, _ammoAmount);
            OnPickup?.Invoke();
            StartCoroutine(destroy());
            _pickedUp = true;
        }
    }


    private IEnumerator destroy()
    {
        yield return new WaitForSeconds(_destroyDelay);
        Destroy(transform.parent.gameObject);
    }
}
