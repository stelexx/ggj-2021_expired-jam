﻿using System.Collections.Generic;
using UnityEngine;

public class RandomSoundPlayer : MonoBehaviour
{
    [SerializeField] private List<AudioClip> _audioClips = new List<AudioClip>();
    [SerializeField] private Vector2 minMaxPitch = new Vector2();
    private AudioSource _audio = null;


    private void Start()
    {
        _audio = GetComponent<AudioSource>();
    }


    public void PlayRandomSoundWithRandomPitch()
    {
        float pitch = Random.Range(minMaxPitch.x, minMaxPitch.y);
        _audio.pitch = pitch;
        _audio.clip = _audioClips[Random.Range(0, _audioClips.Count)];
        _audio.Play();
    }


    public void PlayRandomSound()
    {
        _audio.PlayOneShot(_audioClips[Random.Range(0, _audioClips.Count)]);
    }

    public void PlayWithRandomPitch(AudioClip pClip)
    {
        float pitch = Random.Range(minMaxPitch.x, minMaxPitch.y);
        _audio.pitch = pitch;
        _audio.clip = pClip;
        _audio.Play();
    }
}
