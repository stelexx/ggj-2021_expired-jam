﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour
{
    [SerializeField] private GameObject _popup = null;
    [SerializeField] private PlayerMovement _playerMovement = null;
    [SerializeField] private Slider _musicSlider = null;
    [SerializeField] private Slider _mouseSlider = null;
    [SerializeField] private AudioMixer _mixer = null;


    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if (_popup.activeSelf) DisablePopup();
            else EnablePopup();
        }
    }

    public void DisablePopup()
    {
        _popup.SetActive(false);
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void EnablePopup()
    {
        _popup.SetActive(true);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    
    public void OnMusicSliderChange()
    {
        _mixer.SetFloat("Master", _musicSlider.value);
    }

    public void OnMouseSliderChange()
    {
        _playerMovement.aimSensitivity = _mouseSlider.value;
    }
}
