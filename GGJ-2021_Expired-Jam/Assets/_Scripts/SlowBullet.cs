﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowBullet : Bullet
{
    [SerializeField] private float _slowDuration = 2;
    [SerializeField] private float _slowPercentage = 0.2f;

    private void Start()
    {
        OnHit += OnBulletHit;
    }


    private void OnBulletHit(object pSender, Collision pCollision)
    {
        EnemyPatrolChase enemy = pCollision.gameObject.GetComponent<EnemyPatrolChase>();
        if(enemy != null)
        {
            Debug.Log("Slow enemy!");
            enemy.Slow(_slowDuration, _slowPercentage);
        }
    }
}
